var _spell_manager_8cs =
[
    [ "SpellManager", "class_spell_manager.html", "class_spell_manager" ],
    [ "IEffectInfo", "interface_i_effect_info.html", "interface_i_effect_info" ],
    [ "EffectInfo< SpellClass >", "class_effect_info_3_01_spell_class_01_4.html", "class_effect_info_3_01_spell_class_01_4" ],
    [ "EffectTarget", "_spell_manager_8cs.html#a4394971c5de22fd7a2039a578576ff6a", [
      [ "Offensive", "_spell_manager_8cs.html#a4394971c5de22fd7a2039a578576ff6aa3cf5252b8a7a5c52da62aa4b7fe2cc1e", null ],
      [ "Defensive", "_spell_manager_8cs.html#a4394971c5de22fd7a2039a578576ff6aa3727b4d2311005c88de3b4a52f3de3a2", null ]
    ] ],
    [ "EffectTargetType", "_spell_manager_8cs.html#af54a915d26499d03273e70cdd12c5b4d", [
      [ "Creep", "_spell_manager_8cs.html#af54a915d26499d03273e70cdd12c5b4da0a25a3b81deed37fcd1042c9f0d09a67", null ],
      [ "Lane", "_spell_manager_8cs.html#af54a915d26499d03273e70cdd12c5b4da1a79a39f343f2224748ec987ccf8431f", null ],
      [ "Tower", "_spell_manager_8cs.html#af54a915d26499d03273e70cdd12c5b4da8f8b35b2b5f50b489f1e528f89bdd6e4", null ]
    ] ],
    [ "EffectType", "_spell_manager_8cs.html#a4809a7bb3fd1a421902a667cc1405d43", [
      [ "Instant", "_spell_manager_8cs.html#a4809a7bb3fd1a421902a667cc1405d43a54828f327f31abd59f2f459c0247756d", null ],
      [ "Target", "_spell_manager_8cs.html#a4809a7bb3fd1a421902a667cc1405d43ac41a31890959544c6523af684561abe5", null ]
    ] ]
];