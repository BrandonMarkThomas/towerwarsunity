var class_projectile_controller =
[
    [ "SetTarget", "class_projectile_controller.html#ac7c2bf2466d0454223df47bb7ff78f2a", null ],
    [ "Damage", "class_projectile_controller.html#a4c408fde771587c62c4fba6c4730beca", null ],
    [ "effects", "class_projectile_controller.html#a64c7ea59aeb1cd9769f7395f70f98a64", null ],
    [ "hasTarget", "class_projectile_controller.html#a3f9507d427d8709ce67bc8c702756564", null ],
    [ "Name", "class_projectile_controller.html#abd8f109e4dc45f9e3221a514478aabce", null ],
    [ "Speed", "class_projectile_controller.html#a5c3e4608702d37f3ae7edbc57d987c59", null ],
    [ "target", "class_projectile_controller.html#a3d77d8cf5f1b7d660b45e3637de6c6c3", null ],
    [ "towerManager", "class_projectile_controller.html#a55db14d6bad600c50672bfa62282c100", null ]
];