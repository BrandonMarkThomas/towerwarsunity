var searchData=
[
  ['damage',['Damage',['../class_projectile_controller.html#a4c408fde771587c62c4fba6c4730beca',1,'ProjectileController']]],
  ['deductcost',['DeductCost',['../class_tower_manager.html#ad884fb62724094ba2f165392569c474e',1,'TowerManager']]],
  ['default',['Default',['../_game_controller_8cs.html#aaf5ef5a17b53e9997c837b07015589dea7a1920d61156abc05a60135aefe8bc67',1,'GameController.cs']]],
  ['defensive',['Defensive',['../_spell_manager_8cs.html#a4394971c5de22fd7a2039a578576ff6aa3727b4d2311005c88de3b4a52f3de3a2',1,'SpellManager.cs']]],
  ['description',['Description',['../interface_i_effect_info.html#a0723411d6807e786862594ca7ef6caac',1,'IEffectInfo.Description()'],['../class_effect_info_3_01_spell_class_01_4.html#a0ad4f0a3730c2cebf3e7b441b0f99c2a',1,'EffectInfo&lt; SpellClass &gt;.Description()']]],
  ['disconnectloss',['DisconnectLoss',['../class_consts.html#a273457dda5e979462996f73741ed4126',1,'Consts']]],
  ['disconnectwin',['DisconnectWin',['../class_consts.html#ac57112ace6c71ce5363c56667d880a47',1,'Consts']]]
];
