var class_consts =
[
    [ "DisconnectLoss", "class_consts.html#a273457dda5e979462996f73741ed4126", null ],
    [ "DisconnectWin", "class_consts.html#ac57112ace6c71ce5363c56667d880a47", null ],
    [ "InitialCreepCoolDown", "class_consts.html#a6ddf474aecf332d9deb79b0921c2912c", null ],
    [ "LaneSpace", "class_consts.html#afab561ab35c97afe2d8640a4012811e1", null ],
    [ "LoadReady", "class_consts.html#a1d3194377b8e141d18d72221b9406cca", null ],
    [ "Loss", "class_consts.html#ac937f6dce1ef347dbad366d3166de568", null ],
    [ "MaxPlayerHealth", "class_consts.html#ab11ff25e71e61a8308dfe07a675c0567", null ],
    [ "PauseOther", "class_consts.html#a43d2e1777950370ce03b4b569496d02a", null ],
    [ "PauseOwn", "class_consts.html#a86a165744fb130b1776a4e0588ad6a64", null ],
    [ "TowerRangeIncrease", "class_consts.html#aab18eba68d052b7d6f7145e81c863cbc", null ],
    [ "TowerUpgradeCost", "class_consts.html#a2f29897436d86bc466d6f7a9fb8d4022", null ],
    [ "Waiting", "class_consts.html#aa0851c64385afdaddde3221f80fbd237", null ],
    [ "Win", "class_consts.html#a92ec6a77d142be8831b1329811dcbea2", null ]
];