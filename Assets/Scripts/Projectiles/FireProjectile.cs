﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Controls unique attributes of the fire projectiles
/// </summary>
public class FireProjectile : ProjectileController {

    public GameObject FireEffect;

    /// <summary>
    /// Applies the fire effect
    /// </summary>
	void Start () {
        effects.Add(typeof(FireEffect));
	}
}
