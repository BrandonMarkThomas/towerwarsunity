﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Controls unique attributes of the ice projectiles
/// </summary>
public class IceProjectile : ProjectileController {

    /// <summary>
    /// Applies the Ice effect
    /// </summary>
	void Start () {
        effects.Add(typeof(IceEffect));
	}
}
