﻿using UnityEngine;
using System.Collections;
using Pathfinding;

/// <summary>
/// Handles the interaction for the phantom creep (a phantom creep is used to check whether the lane has been blocked by a tower).
/// </summary>
public class PhantomCreepController : MonoBehaviour {

	public int 					index;
	public GameObject			towerInstance;
	public TowerManager			towerManager;
	public bool 				validPath 		= false;

	// Pathfinding variables
	public Seeker 				seeker;
	public Vector3 				targetPosition;
	public CharacterController 	controller;
	protected Transform 		goalArea;
	private Path 				path;

	/// <summary>
	/// Sets the goal area that relates to this creep.
	/// </summary>
	/// <param name="goalArea">Goal area.</param>
	/// <param name="setPath">If pathfinding should be set for this creep.</param>
	public void SetGoal(Transform goalArea, bool setPath)
	{
		this.goalArea = goalArea;
		targetPosition = goalArea.position;
		GetNewPath();
	}
	
	/// <summary>
	/// Calculates the path from the creeps current position to the target position in the goal area.
	/// </summary>
	public void GetNewPath()
	{
		AstarPath.active.Scan ();
		seeker.StartPath(transform.position, targetPosition + Vector3.back, OnPathComplete);
	}
	
	/// <summary>
	/// Raises the path complete event.
	/// </summary>
	/// <param name="newPath">New path.</param>
	void OnPathComplete(Path newPath)
	{
		if (!newPath.error)
		{
			path = newPath;
			CheckPathCompletion();
		}
		towerManager.CatchPhantom (towerInstance, this.gameObject);

	}
	
	/// <summary>
	/// Checks the path completion and determines whether the lane has been blocked.
	/// </summary>
	void CheckPathCompletion()
	{
		validPath = true;
		float difference = 0.05f;
		if ((targetPosition - path.vectorPath[path.vectorPath.Count - 1]).sqrMagnitude >= (targetPosition * difference).sqrMagnitude)
		{
			validPath = false;
		}
	}
}
