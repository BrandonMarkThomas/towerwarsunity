﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles the synchronization of creeps across the network. 
/// </summary>
public class CreepNetworker : MonoBehaviour {

	GameController gameController;
	PhotonView view;
	private float lastSyncTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPos;
	private Vector3 syncEndPos = Vector3.zero;

	/// <summary>
	/// Sets the game controller and photon view.
	/// </summary>
	void Awake()
	{
		gameController = GameObject.Find("GameController").GetComponent<GameController>();
		view = GetComponent<PhotonView>();
	}

	/// <summary>
	/// Raises the photon instantiate event and moves the creep to the right place in the object hierarchy.
	/// </summary>
	/// <param name="info">Info.</param>
	void OnPhotonInstantiate(PhotonMessageInfo info)
	{
		// if this object creep gameobject was instantiated by the other client
		if (!view.isMine)
		{
			// move this game object to the right place in the object hierarchy
			this.gameObject.transform.parent = gameController.TargetPlayerController.Lane.CreepObjs.transform;
		}
		syncEndPos = this.gameObject.rigidbody.position;
	}

	/// <summary>
	/// Updates the position of the creep.
	/// </summary>
	void Update(){
		if(!view.isMine){
			syncTime += Time.deltaTime;
			if (syncDelay == 0) return;	// avoid divide by 0 errors
			rigidbody.position = Vector3.Lerp(syncStartPos, syncEndPos, syncTime/syncDelay);
		}
	}

	/// <summary>
	/// Raises the photon serialize view event and sets the sync positions and time.
	/// </summary>
	/// <param name="stream">Stream data<param>
	/// <param name="info">Info data</param>
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
			stream.SendNext(rigidbody.position);
		else{
			syncEndPos = (Vector3)stream.ReceiveNext();
			syncStartPos = this.gameObject.rigidbody.position;
			
			syncTime = 0f;
			syncDelay = Time.time - lastSyncTime;
			lastSyncTime = Time.time;

		}
	}

}
