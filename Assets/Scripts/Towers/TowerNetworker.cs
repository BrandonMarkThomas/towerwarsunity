﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains synchronisation of tower projectiles and instantiations
/// </summary>
public class TowerNetworker : MonoBehaviour {

	GameController gameController;
	TowerController towerController;
	PhotonView view;

    /// <summary>
    /// Sets up the gamecontroller, towercontroller and gets the photon view
    /// </summary>
	void Awake()
	{
		gameController = GameObject.Find("GameController").GetComponent<GameController>();
		towerController = GetComponent<TowerController>();
		view = GetComponent<PhotonView>();
	}

    /// <summary>
    /// Calls for a projectile to be created/fired based on online/offline modes
    /// </summary>
    /// <param name="position"></param>
    /// <param name="target"></param>
	public void FireProjectile(Vector3 position, GameObject target)
	{
		if (gameController.Offline)
			Spawn(position, target);
		else
		{
			if (!view.isMine) return;
			view.RPC("SynchSpawn", PhotonTargets.All, new object[] { position, target.GetPhotonView().viewID });
		}
	}

    /// <summary>
    /// Synchronises the instantiation of projectiles over the photon network
    /// </summary>
    /// <param name="position"></param>
    /// <param name="targetID"></param>
	[RPC]
	void SynchSpawn(Vector3 position, int targetID)
	{
		PhotonView targView = PhotonView.Find(targetID);
		Spawn(position, targView.gameObject);
	}

    /// <summary>
    /// Instantiates the projectile at the position
    /// </summary>
    /// <param name="position"></param>
    /// <param name="target"></param>
	void Spawn(Vector3 position, GameObject target)
	{
		ProjectileController controller = ((GameObject)Instantiate(towerController.Projectile, position, Quaternion.identity)).GetComponent<ProjectileController>();
		controller.SetTarget(target.transform, towerController.Manager);
	}

    /// <summary>
    /// Sets the owner of the instantiated projectiles to avoid confusion/conflicts
    /// </summary>
    /// <param name="info"></param>
	void OnPhotonInstantiate(PhotonMessageInfo info)
	{
		// if this object creep gameobject was instantiated by the other client
		if (!view.isMine)
		{
			// move this game object to the right place in the object hierarchy, and set the manager object
			this.gameObject.transform.parent = gameController.TargetPlayerController.Lane.TowerObjs.transform;
			towerController.Manager = gameController.TargetPlayerController.Towers;
		}
	}

	// TODO: sync rotation or something?
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{

	}
}
