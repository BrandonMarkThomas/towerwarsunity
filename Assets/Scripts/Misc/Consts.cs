﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class to store constant strings and values used in the project. TODO: Move more constants into here/change implementation?
/// </summary>
public static class Consts {

	// Pause menu strings
	public const string DisconnectLoss = "Connection lost! Game over.";
	public const string DisconnectWin = "Other player disconnected. You won!";
	public const string Loss = "You lost!";
	public const string PauseOther = "Other player paused the game";
	public const string PauseOwn = "You paused the game";
	public const string Waiting = "Waiting for other player to load...";
	public const string Win = "You won!";

	// Custom properties for PhotonPlayer
	public const string LoadReady = "Loadready";

	// space between lanes (on x axis)
	public const int LaneSpace = 30;

	public const int MaxPlayerHealth = 20;

	// amount of time before creeps can be sent at the start of a game
	public const int InitialCreepCoolDown = 5;

	// temporary constants for tower upgrades; should be refactored better in future
	public const int TowerUpgradeCost = 10;
	public const float TowerRangeIncrease = 1.3f;
}
