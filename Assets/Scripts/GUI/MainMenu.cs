﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enum used to indicate what stage of the menu it is at. Could possibly be extended to include some more multiplayer states?
/// </summary>
public enum MenuMode
{
	Home,
	Multiplayer,
}

/// <summary>
/// Class that controls the main menu scene. Handles multiplayer set up interaction, and starts any of the game modes.
/// </summary>
public class MainMenu : MonoBehaviour
{
	public MenuMode mode;

	public float widthPercent = 0.8f;
	public float heightPercent = 0.8f;

	public bool prevMulti = false;

	public GUISkin skin;

	private string menuGameName = "Player's Game";
	private Vector2 roomListScroll;
	public const float refreshConst = 5.0f;
	private float refreshTimer;

	private MainController main;
	private NetworkController net;

	/// <summary>
	/// Initialisation
	/// </summary>
	void Start()
	{
		GameObject mainObj = GameObject.Find("MainController");
		main = mainObj.GetComponent<MainController>();
		net = main.NetworkControl;
		mode = MenuMode.Home;
	}

	/// <summary>
	/// Draws the GUI elements constantly.
	/// </summary>
	void OnGUI()
	{
		GUI.skin = skin;
		Rect r = new Rect(Screen.width * (1 - widthPercent) / 2, Screen.height * (1 - heightPercent) / 2, Screen.width * widthPercent, Screen.height * heightPercent);

		GUILayout.BeginArea(r);
		GUILayout.BeginVertical("window");

		GUILayout.Label("Tower Wars");

		// main menu
		if (mode == MenuMode.Home)
		{
			if (GUILayout.Button("Singleplayer"))
			{
				net.LoadGame(true);
			}

			if (GUILayout.Button("Versus AI"))
			{
				main.AIMode = true;
				net.LoadGame(true);
			}

			if (GUILayout.Button("Multiplayer"))
			{
				mode = MenuMode.Multiplayer;
			}

			if (GUILayout.Button("Quit"))
			{
				Application.Quit(); // doesn't work in the Unity Editor; only built versions
			}
			//	GUILayout.BeginHorizontal();
			//	GUILayout.Label("Enter your name here", GUILayout.ExpandWidth(true));
			//	playerName = GUILayout.TextField(playerName, 25, GUILayout.ExpandWidth(true));
			//	GUILayout.EndHorizontal();
		}
		else
		{
			// first time after multiplayer clicked - connect to Photon Network
			if (prevMulti == false)
			{
				if (!net.IsConnected())
				{
					net.Connect();
				}
				prevMulti = true;
			}

			// if client is joining/creating a room
			if (net.IsJoiningRoom)
			{
				GUILayout.Label("Please wait...");
			}
			else
			{
				if (!net.InRoom())
				{
					if (!net.IsConnected())
					{
						GUILayout.Label(net.ConnectStatus());
					}
					else
					{
						GUILayout.Label("Available game rooms");
						roomListScroll = GUILayout.BeginScrollView(roomListScroll);
						RoomInfo[] rooms = net.GetRooms();
						if (rooms != null)
						{
							for (int i = 0; i < rooms.Length; i++)
							{
								if (GUILayout.Button(rooms[i].name))
								{
									net.JoinRoom(rooms[i].name);
								}
							}
						}
						GUILayout.EndScrollView();
						if (GUILayout.Button("Refresh"))
						{
							net.Refresh();
						} GUILayout.BeginHorizontal();
						if (GUILayout.Button("Create room"))
						{
							net.CreateRoom(menuGameName);
						}
						menuGameName = GUILayout.TextField(menuGameName);
						GUILayout.EndHorizontal();
					}
					if (GUILayout.Button("Back"))
					{
						mode = MenuMode.Home;
					}
				}
				else
				{
					GUILayout.Label(net.getRoomName());
					if (net.RoomFull())
					{
						GUILayout.Label("Game ready.");
						if (net.IsRoomMaster())
						{
							if (GUILayout.Button("Start game"))
							{
								net.LoadGame(false);
							}
						}
						else
						{
							GUILayout.Label("Waiting for host to start game...");
						}
					}
					else
					{
						GUILayout.Label("Waiting for another player...");
					}
					if (GUILayout.Button("Back"))
					{
						net.LeaveRoom();
					}
				}
			}
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();

		// Handles the automatic refresh every refreshConst seconds.
		if (Event.current.type == EventType.Repaint)
		{
			if (net.IsConnected() && !net.InRoom())
			{
				refreshTimer -= Time.deltaTime;
				if (refreshTimer <= 0)
				{
					net.Refresh();
					refreshTimer = refreshConst;
				}
			}
		}
	}
}
