using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Enum used to keep track of what 'game mode' the user is currently in. Each mode will present the GUI differently/have listeners on lane accordingly, etc.
/// </summary>
public enum GameMode
{
	Default,
	Construction,
	Spells
}

/// <summary>
/// Enum used to keep track of what mode relating to tower construction the user is currently in.
/// </summary>
public enum TowerMode
{
	Build,
	Sell,
	Upgrade,
}

/// <summary>
/// Enum used to classify what state the game is in if the game is paused. This is used to vary the pause menu accordingly.
/// </summary>
public enum PauseState
{
	Loaded,
	Started,
	Finished
}

/// <summary>
/// Enum used to represent a specific player in the game. Ideally this could be extended/replaced with PhotonPlayer objects in future.
/// </summary>
public enum Player : int
{
	Zero = 0,	// used to indicate unassigned
	One = 1,
	Two = 2
}

/// <summary>
/// Main overhead class for controlling a game. Only one instance on each client. All RPCs should be declared in here.
/// </summary>
public class GameController : MonoBehaviour
{
	public MainController Main { get; private set; }
	public PrefabController Prefabs { get; private set; }
	public GUIController GUI { get; private set; }
	public Player CurrentPlayer = Player.Zero;
	public Player TargetPlayer = Player.Zero;

	public Dictionary<Player, GameObject> Players;
	public PlayerController CurrentPlayerController { get; private set; }
	public PlayerController TargetPlayerController { get; private set; }

	// Lane and Player prefab should maybe be in PrefabController?
	public GameObject LanePrefab;
	public GameObject PlayerPrefab;
	public GameObject MainCamera;			// cameras set in inspector
	public GameObject OtherLaneCamera;

    // Smoke and mirrors
    public GameObject FireEffect;

	public PhotonView View;

	public bool Paused = true;							// simple flag to hold if game is paused or not
	public PauseState GameState = PauseState.Loaded;

	public bool Offline;
	public bool CreepsTakeDamage = true;	// for debugging purposes

	public bool InitialCreepCoolDown = true;

	public AIController AI;

	/// <summary>
	/// Initialisation of script
	/// </summary>
	void Awake()
	{
		Main = GameObject.Find("MainController").GetComponent<MainController>();	// will carry over from MainMenu scene
		Prefabs = GetComponent<PrefabController>();
		GUI = GetComponent<GUIController>();

		Players = new Dictionary<Player, GameObject>();		// Maps player enum to a gameobject that contains the controllers for that player

		Offline = PhotonNetwork.offlineMode;				// Offline = Singleplayer mode
		View = GetComponent<PhotonView>();

		AddPlayer(Player.One);
		AddPlayer(Player.Two);

		Player curr, targ;
		if (Main.NetworkControl.IsRoomMaster())
		{
			curr = Player.One;
			targ = Player.Two;
		}
		else
		{
			curr = Player.Two;
			targ = Player.One;
		}
		SetPlayers(curr, targ);

		if (Main.AIMode)
		{
			AI = this.gameObject.AddComponent<AIController>();
		}
		PauseLocal(Consts.Waiting);
		ReadyToStart();
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		if (GameState == PauseState.Finished) return;

		if (Input.GetKeyDown(KeyCode.Escape))
			if (Paused)
			{
				UnPause();
			}
			else
			{
				Pause(Consts.PauseOwn);
			}
	}

	/// <summary>
	/// Method used to indicate that this client is ready to start the game.
	/// </summary>
	void ReadyToStart()
	{
		if (Offline)
		{
			// just unpause
			UnPause();
		}
		else
		{
			// otherwise signal that this client is ready (via custom properties of this client's PhotonPlayer object)
			PhotonNetwork.player.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() { { Consts.LoadReady, true } });
			View.RPC("CheckReady", PhotonTargets.MasterClient);
		}
	}

	/// <summary>
	/// RPC method called by ReadyToStart. This is only called on the MasterClient (each time any client is ready), and checks the ready status of all clients in the game room.
	/// </summary>
	[RPC]
	void CheckReady()
	{
		if (!PhotonNetwork.isMasterClient) return; // shouldn't ever happen in theory (RPC only sent to MasterClient)

		// loop through player list and see if everyone's ready - if not, we don't need to do anything just yet
		foreach (PhotonPlayer player in PhotonNetwork.playerList)
		{
			if (!(bool)player.customProperties[Consts.LoadReady])
			{
				return;
			}
		}
		// otherwise, start the game
		// currently just starts by unpausing - could be improved to include things like countdown timer?
		UnPause();
	}

	/// <summary>
	/// Pauses the game. Consequently calls PauseLocal locally or via RPC depending on if the game is multiplayer.
	/// </summary>
	/// <param name="msg">Relevant pause message (displayed to user)</param>
	public void Pause(string msg)
	{
		if (Offline)
		{
			PauseLocal(msg);
		}
		else
		{
			View.RPC("PauseLocal", PhotonTargets.AllViaServer, new object[] { msg, (int)CurrentPlayer });
		}
	}

	/// <summary>
	/// Pauses the game on this client.
	/// </summary>
	/// <param name="msg">Relevant pause message</param>
	void PauseLocal(string msg)
	{
		PauseLocal(msg, (int)CurrentPlayer);
	}

	/// <summary>
	/// Pauses the game on this client.
	/// </summary>
	/// <param name="msg">Relevant pause message</param>
	/// <param name="player">The player that instigated the pause</param>
	[RPC]
	void PauseLocal(string msg, int player)
	{
		// if this player did not initiate the pause, the other player must have done so; change the message accordingly
		if (msg == Consts.PauseOwn && (Player)player != CurrentPlayer) msg = Consts.PauseOther;
		Paused = true;
		GUI.PauseMessage = msg;
		Time.timeScale = 0;
	}

	/// <summary>
	/// Unpauses the game on this client by calling UnPauseLocal locally or via RPC accordingly.
	/// </summary>
	public void UnPause()
	{
		if (Offline)
		{
			UnPauseLocal();
		}
		else
		{
			View.RPC("UnPauseLocal", PhotonTargets.AllViaServer);
		}
	}

	/// <summary>
	/// Unpauses the game on this client.
	/// </summary>
	[RPC]
	void UnPauseLocal()
	{
		Paused = false;
		Time.timeScale = 1;
		// if this is the first unpause, game has just started
		if (GameState == PauseState.Loaded)
		{
			GameState = PauseState.Started;
			StartCoroutine(StartInitialCooldown(Consts.InitialCreepCoolDown));
		}
	}

	/// <summary>
	/// Instantiates a player prefab that has all the necessary controllers for that player.
	/// </summary>
	/// <param name="playerName">Player enum value of the player being created.</param>
	void AddPlayer(Player playerName)
	{
		GameObject player = Instantiate(PlayerPrefab) as GameObject;
		player.name = "Player" + playerName;
		PlayerController playerController = player.GetComponent<PlayerController>();
		Vector3 lanePosition = new Vector3(((int)playerName - 1) * Consts.LaneSpace, 0, 0);
		playerController.Player = playerName;
		playerController.CreateLane(LanePrefab, lanePosition);
		playerController.SetGameMode(playerController.GMode);
		Players.Add(playerName, player);
	}

	/// <summary>
	/// Set the current and target player for this game controller.
	/// </summary>
	/// <param name="current">The player that the user is currently controlling.</param>
	/// <param name="target">The player that the user is playing against.</param>
	public void SetPlayers(Player current, Player target)
	{
		CurrentPlayer = current;
		CurrentPlayerController = Players[CurrentPlayer].GetComponent<PlayerController>();
		CurrentPlayerController.Lane.PlacementController.OnPlacementSelect += new PlacementEventHandler(HandlePlacementSelect);

		TargetPlayer = target;
		TargetPlayerController = Players[TargetPlayer].GetComponent<PlayerController>();

		// Player one's lane starts at x=0, other lanes will be in multiples of a constant LaneSpace value
		MainCamera.transform.position = new Vector3(((int)CurrentPlayer - 1) * Consts.LaneSpace, MainCamera.transform.position.y, MainCamera.transform.position.z);
		OtherLaneCamera.transform.position = new Vector3(((int)TargetPlayer - 1) * Consts.LaneSpace, OtherLaneCamera.transform.position.y, OtherLaneCamera.transform.position.z);
	}

	/// <summary>
	/// Current player chooses to send a creep to the target player.
	/// </summary>
	/// <param name="creepIndex">Index (in the creep prefab list) of the sent creep</param>
	public void SendCreep(int creepIndex)
	{
		// check that there is space in stack and has enough funds.
		// creep stack should be bound to this player controller
		if (!CurrentPlayerController.Creeps.CheckCanSpawn(creepIndex, true))
		{
			Debug.Log("Creep could not be spawned");
			return;
		}

		// creep can be sent; spawn it on the target player's lane
		if (Offline)
		{
			SpawnCreep(creepIndex, (int)TargetPlayer);
		}
		else
		{
			// Can set PhotonTarget to others as the other client will be the one instantiating.
			// If in future development more players are added, this should just be sent to the target PhotonPlayer.
			View.RPC("SpawnCreep", PhotonTargets.Others, new object[] { creepIndex, (int)TargetPlayer });
		}
	}

	/// <summary>
	/// Spawns a creep on a given player's lane.
	/// In multiplayer, only the targeted player will run this method and network instantiate the creep on their lane.
	/// </summary>
	[RPC]
	public void SpawnCreep(int creepIndex, int playerToReceive)
	{
		PlayerController playerController = Players[(Player)playerToReceive].GetComponent<PlayerController>();
		Vector3 position = playerController.Lane.GetCreepSpawnPosition();
		GameObject creep = playerController.Creeps.SpawnCreep(creepIndex, position);
		playerController.Lane.SetCreep(creep);
	}

	/// <summary>
	/// Called when a creep reaches the goal area at the end of a lane. Will call the UpdatePlayerHealth method accordingly.
	/// </summary>
	/// <param name="creep">The creep gameobject that reached the goal area.</param>
	public void CreepGoalReached(GameObject creep)
	{
		if (Offline)
		{
			UpdatePlayerHealth((int)creep.GetComponent<CreepController>().Manager.PlayerController.Player);
		}
		else
		{
			View.RPC("UpdatePlayerHealth", PhotonTargets.All, new object[] { (int)CurrentPlayer });
		}
		RemoveCreep(creep, false);
	}

	/// <summary>
	/// Deducts the health of a player by 1, and checks if the game is now consequently over (i.e. player has no health left)
	/// </summary>
	/// <param name="player">The player whose health is being deducted.</param>
	[RPC]
	public void UpdatePlayerHealth(int player)
	{
		PlayerController playerController = Players[(Player)player].GetComponent<PlayerController>();
		playerController.PlayerInfo.Health--;
		if (playerController.PlayerInfo.Health <= 0)
		{
			SendGameOver(player);
		}
	}

	/// <summary>
	/// Called the game is deemed to be over; will call the GameOver method appropriately.
	/// </summary>
	/// <param name="player"></param>
	public void SendGameOver(int player)
	{
		if (Offline)
		{
			GameOver(player);
		}
		else
		{
			// ensure the RPC is only called by one client
			if ((int)CurrentPlayer == player)
			{
				View.RPC("GameOver", PhotonTargets.AllViaServer, new object[] { player });
			}
		}
	}

	/// <summary>
	/// The game is over. Set the game state to be paused and finished.
	/// </summary>
	/// <param name="playerNum">The player who lost the game.</param>
	[RPC]
	public void GameOver(int playerNum)
	{
		CurrentPlayerController.Achievements.PrintAchievements();
		string finishString = (playerNum == (int)CurrentPlayer) ? Consts.Loss : Consts.Win;
		GameState = PauseState.Finished;
		PauseLocal(finishString);
	}

	/// <summary>
	/// Removes a creep gameobject from the game. Will give current player money if this creep was removed via a tower hit.
	/// </summary>
	/// <param name="creep">Creep gameobject</param>
	/// <param name="creepHit">Flag to indicate if this creep was removed via a tower hit</param>
	public void RemoveCreep(GameObject creep, bool creepHit)
	{
		PhotonView view = creep.GetPhotonView();
		if (view.isMine)
		{
			if (creepHit)
			{
				// increase player money each time a creep is killed
				Debug.Log("added: " + (int)Math.Ceiling(creep.GetComponent<CreepController>().Cost * 0.33));
				creep.GetComponent<CreepController>().Manager.PlayerController.PlayerInfo.Money += (int)Math.Ceiling(creep.GetComponent<CreepController>().Cost * 0.33);
				creep.GetComponent<CreepController>().Manager.PlayerController.Achievements.IncrementCreepsKilled(creep.GetComponent<CreepController>().prefabIndex);
			}
			// destroy creep
			PhotonNetwork.Destroy(creep);
		}
	}

	/// <summary>
	/// A creep has just been hit by a projectile on this lane. 
	/// </summary>
	/// <param name="creep">Creep gameobject that was hit.</param>
	/// <param name="projectileDamage">Damage of the projectile that hit the creep.</param>
	public void CreepHitByProj(GameObject creep, int projectileDamage)
	{
		if (!CreepsTakeDamage) return;
		if (Offline)
		{
			CreepHit(creep, projectileDamage);
		}
		else
		{
			if (creep == null) return;
			PhotonView creepView = creep.GetComponent<PhotonView>();
			if (!creepView.isMine) return;					// Don't do anything if the creep hit occurred locally on the other lane; the client in charge of that lane will be the only one to signal the hit
			int viewID = creepView.viewID;	
			View.RPC("CreepHit", PhotonTargets.All, new object[] { viewID, projectileDamage });
		}
	}

	/// <summary>
	/// Offline version of the creep hit event. Gets the CreepController script attached to the hit creep, and calls the post-hit method.
	/// </summary>
	/// <param name="creep">Creep gameobject that was hit.</param>
	/// <param name="projectileDamage">Damage of the projectile that hit the creep.</param>
	public void CreepHit(GameObject creep, int projectileDamage)
	{
		CreepController creepController = creep.GetComponent<CreepController>();
		CreepHitPost(creepController, projectileDamage);
	}

	/// <summary>
	/// Multiplayer version of the creep hit method. Since gameobjects cannot be passed over the network, the ViewID of the PhotonView component on the gameobject is passed over instead.
	/// The game object can subsequently be found on the on the receiving client with this ID.
	/// </summary>
	/// <param name="creepViewID">ViewID of the PhotonView on the creep being hit.</param>
	/// <param name="projectileDamage">Damage of the projectile that hit the creep.</param>
	[RPC]
	public void CreepHit(int creepViewID, int projectileDamage)
	{
		PhotonView creepView = PhotonView.Find(creepViewID);
		CreepController creepController = creepView.GetComponent<CreepController>();
		CreepHitPost(creepController, projectileDamage);
	}

	/// <summary>
	/// Post method of a creep hit. Removes the projectile damage from the health of the creep, and checks to see if creep has "died" - i.e. has lost all health
	/// </summary>
	/// <param name="creepController">CreepController component of the creep that was hit.</param>
	/// <param name="projectileDamage">Damage of the projectile that hit the creep.</param>
	public void CreepHitPost(CreepController creepController, int projectileDamage)
	{
		creepController.Health -= projectileDamage;

		if (creepController.Health <= 0)
		{
			RemoveCreep(creepController.gameObject, true);
		}
	}

	/// <summary>
	/// Signals that a tower is wishing to be built at a given position on the lane. Checks if it is possible - if so, starts the build process.
	/// </summary>
	/// <param name="towerIndex">Index (in the tower prefab list) of the tower to be built.</param>
	/// <param name="position">Position of the placement tile that the tower is proposed to be built on.</param>
	public void BuildTower(int towerIndex, Vector3 position)
	{
		TowerController towerControllerPrefab = Prefabs.Towers[towerIndex].GetComponent<TowerController>();
		if (CurrentPlayerController.Towers.CheckCanBuild(towerIndex))
		{
			CurrentPlayerController.Towers.BuildPhantom(towerIndex, position);
		}
		else
		{
			GUI.ShowInvalidMoney();
		}
	}

	/// <summary>
	/// Cuyrrent player has chosen to sell a specific tower. Calls the remove method in the tower manager and gives some money (half of the tower cost) back to the player.
	/// </summary>
	/// <param name="tower">The tower gameobject that is being sold.</param>
	public void SellTower(GameObject tower)
	{
		CurrentPlayerController.Towers.RemoveTower(tower.transform.position);
		CurrentPlayerController.PlayerInfo.Money += (int)(tower.GetComponent<TowerController>().Cost * 0.5);
	}

	/// <summary>
	/// Player has chosen to upgrade a specific tower. For now, just increases the range of the tower and colours it's dome green.
	/// </summary>
	/// <param name="tower">The tower gameobject that is being upgraded.</param>
	public void UpgradeTower(GameObject tower)
	{
		// TODO: check implementation with effects class?
		if (CurrentPlayerController.Towers.CheckCanUpgrade(tower, Consts.TowerUpgradeCost))
		{
			CurrentPlayerController.PlayerInfo.Money -= Consts.TowerUpgradeCost;
			tower.GetComponent<SphereCollider>().radius *= Consts.TowerRangeIncrease;
			Transform turret = tower.transform.Find("Base/Turret");
			turret.GetComponent<Renderer>().material.color = Color.green;
			tower.GetComponent<TowerController>().Upgraded = true;
		}
		else
		{
			Debug.Log("Nope");
		}
	}

	/// <summary>
	/// Toggles the current and target players. (Used for Singleplayer [debug] mode.)
	/// </summary>
	public void TogglePlayers()
	{
		SetPlayers(TargetPlayer, CurrentPlayer);
		GUI.GameModeIndex = (int)CurrentPlayerController.GMode - 1;
		GUI.TowerModeIndex = (int)CurrentPlayerController.TMode - 1;
	}

	/// <summary>
	/// Event handler for dealing with clicking on a placement tile in the current player's lane. Calls the appropriate tower construction method.
	/// </summary>
	/// <param name="placementTile">Placement tile clicked.</param>
	void HandlePlacementSelect(Transform placementTile)
	{
		if (Paused) return;
		if (CurrentPlayerController.GMode != GameMode.Construction) return;

		// check that this is the current player's lane
		if (CurrentPlayer != placementTile.GetComponentInParent<LaneController>().Player) return;

		Vector3 position = placementTile.position;
		// check if a tower already exists in this space
		GameObject existingTower = CurrentPlayerController.Towers.GetExisting(position);

		TowerMode mode = CurrentPlayerController.TMode;

		if (mode == TowerMode.Build && existingTower == null)
		{
			BuildTower(GUI.TowerIndex, placementTile.position);
		}
		else if (existingTower != null)
		{
			if (mode == TowerMode.Sell)
			{
				SellTower(existingTower);
			}
			else if (mode == TowerMode.Upgrade)
			{
				UpgradeTower(existingTower);
			}
		}
	}

	/// <summary>
	/// Photon event method. Other player has disconnected, therefore end the game, with the current player as the winner.
	/// </summary>
	/// <param name="player"></param>
	void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		if (GameState != PauseState.Finished)
		{
			PauseLocal(Consts.DisconnectWin);
			GameState = PauseState.Finished;
		}
	}

	/// <summary>
	/// Photon event method. This client has lost connection to the Photon Network - end the game, with the current player as the loser.
	/// </summary>
	void OnDisconnectedFromPhoton()
	{
		if (GameState != PauseState.Finished)
		{
			PauseLocal(Consts.DisconnectLoss);
			GameState = PauseState.Finished;
		}
	}

	/// <summary>
	/// IEnumerator used to countdown the time before creeps can be sent.
	/// </summary>
	/// <param name="time">Time (in seconds) to count down.</param>
	/// <returns>When time has counted down to zero</returns>
	IEnumerator StartInitialCooldown(int time)
	{
		while (time > 0)
		{
			GUI.CoolDownMessage = "Available in " + time + " second" + ((time == 1) ? "" : "s") + "...";
			yield return new WaitForSeconds(1);
			time -= 1;
		}
		InitialCreepCoolDown = false;
	}

	/// <summary>
	/// A spell has been cast but it affects objects on the target player's lane - this RPC is used to send the target client all the gameobjects that are affected.
	/// </summary>
	/// <param name="spellIndex">Index of the spell cast</param>
	/// <param name="viewIDs">Array of viewIDs of gameobjects that have been affected by this spell cast</param>
	[RPC]
	public void RemoteCast(int spellIndex, int[] viewIDs)
	{
		IEffectInfo info = TargetPlayerController.Spells.AvailableSpells [spellIndex];

		foreach (int viewID in viewIDs) 
		{
			PhotonView target = PhotonView.Find (viewID);

			if(target.isMine)
			{
				info.Cast(target.gameObject);
			}
		}
	}
}

