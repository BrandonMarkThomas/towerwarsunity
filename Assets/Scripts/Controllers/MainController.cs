﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The main "bootstrap" class used in the project. This is attached to the MainContrller GameObject in the start scene, and won't be destroyed at any point. 
/// Primarily implemented for the transition between the main menu and game scenes.
/// </summary>
public class MainController : MonoBehaviour
{
	public NetworkController NetworkControl { get; private set; }
	public bool AIMode = false;

	void Awake()
	{
		DontDestroyOnLoad(this);
		NetworkControl = GetComponent<NetworkController>();
		Application.LoadLevel("MainMenu");
	}
}