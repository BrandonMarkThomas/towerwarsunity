﻿using UnityEngine;
using System.Collections;
using Pathfinding;

/// <summary>
/// Handles the interaction of an individual creep. Cost, health, speed etc.
/// </summary>
public class CreepController : MonoBehaviour
{

	private bool isLaneBlocked 	= false;
	private bool isPathCompleted = false;
	public int prefabIndex = 0;
	public string Name;
	public int Cost;
	public int MaxHealth;
	public int Health { get; set; }
	public float Speed = 0.001f;
	public int CoolDownMax;
	public float CoolDownRate;

	public float RuntimeSpeed { get; set; }
	
	public Material material;
	public CreepManager Manager;
	private GameObject Tower;

	// Pathfinding variables
	public Seeker seeker;
	public Vector3 targetPosition;
	public CharacterController controller;
	protected Transform goalArea;
	private Path path;
	private const float nextWayPoint = 0.5f;
	private int currentPoint = 0;
	
	private double slowTime;
	private double burnTime;
	
	private int secondsPassed;

	/// <summary>
	/// Start this creep instance and set the initial creep health.
	/// </summary>
	public void Start()
	{
		Health = MaxHealth;
	}

	/// <summary>
	/// Update this instance and reset all runtime stats to base
	/// </summary>
	public void Update()
	{
		// Reset all runtime stats to base
		RuntimeSpeed = Speed;
	}
	
	/// <summary>
	/// Sets the goal area that relates to this creep.
	/// </summary>
	/// <param name="goalArea">Goal area.</param>
	/// <param name="setPath">If pathfinding should be set for this creep.</param>
	public void SetGoal(Transform goalArea, bool setPath)
	{
		this.goalArea = goalArea;
		targetPosition = goalArea.position;
		if (setPath)
			GetNewPath();
	}

	/// <summary>
	/// Calculates the path from the creeps current position to the target position in the goal area.
	/// </summary>
	public void GetNewPath()
	{
		isPathCompleted = false;
		AstarPath.active.Scan ();
		seeker.StartPath(transform.position, targetPosition + Vector3.back, OnPathComplete);
		return;	
	}

	/// <summary>
	/// Raises the path complete event.
	/// </summary>
	/// <param name="newPath">New path.</param>
	void OnPathComplete(Path newPath)
	{
		if (!newPath.error)
		{
			path = newPath;
			currentPoint = 0;
			CheckPathCompletion();
		}
		isPathCompleted = true;
	}
	
	/// <summary>
	/// Checks the path completion and determines whether the lane has been blocked.
	/// </summary>
	void CheckPathCompletion()
	{
		isLaneBlocked = false;
		float difference = 0.05f;
		if ((targetPosition - path.vectorPath[path.vectorPath.Count - 1]).sqrMagnitude >= (targetPosition * difference).sqrMagnitude)
		{
			isLaneBlocked = true;
		}
	}
	
	/// <summary>
	/// Updates the creep position based on pathfinding.
	/// </summary>
	void FixedUpdate()
	{
		if (isPathCompleted) {
			// If a path has not been calculated
			if (path == null) {
				return;
			}
			
			// If path is blocked before creep is spawned
			if (currentPoint >= path.vectorPath.Count /*&& isMoving*/) {
				GetNewPath ();
				if (isLaneBlocked) {
					RemoveTower ();
					GetNewPath ();
					
				}
				return;
			}
			
			// Move the creep further along the path
			Vector3 dir = (path.vectorPath [currentPoint] - transform.position).normalized;
			dir *= RuntimeSpeed * Time.fixedDeltaTime;
			controller.SimpleMove (dir);
			
			// Move the current position further along the path
			if (Vector3.Distance (transform.position, path.vectorPath [currentPoint]) < nextWayPoint) {
				currentPoint++;
				return;
			} 
			
			// If path is blocked after creep is spawned
			if (controller.velocity.sqrMagnitude < .01 /*&& isMoving*/) {
				GetNewPath ();
				return;
			}
		}
	}
	
	/// <summary>
	/// Raises the disable event.
	/// </summary>
	public void OnDisable () {
		seeker.pathCallback -= OnPathComplete;
	} 
	
	/// <summary>
	/// Finds the position of the tower that is blocking the creeps path, removes the tower from the map, destroys the gameobject and updates the grid for pathfinding.
	/// </summary>
	void RemoveTower()
	{
		// Find the position of the tower that is blocking the creeps path
		Vector3 temp = transform.position;
		Vector3 pos = new Vector3(Mathf.Round(temp.x), Mathf.Round(temp.y), Mathf.Round(temp.z) - 1f);
		GameObject tower = Manager.PlayerController.Towers.GetExisting(pos);

		if (tower != null)
		{
			// Update the grid node, and remove the tower
			tower.transform.GetChild(0).gameObject.layer = 3;
			AstarPath.active.UpdateGraphs(tower.transform.GetChild(0).gameObject.collider.bounds);
			Manager.PlayerController.Game.SellTower(tower);
		}
	}
}

