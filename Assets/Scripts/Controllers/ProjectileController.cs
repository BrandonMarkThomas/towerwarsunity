﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// Handles the interaction of individual projectiles. Projeciles are created locally on each client, and only the spawn and hit events will be synchronised (i.e. not the movement)
/// </summary>
public class ProjectileController : MonoBehaviour {

	public string Name;
	public int Damage;
	public float Speed;
	protected TowerManager towerManager;

	protected Transform target;
	protected bool hasTarget = false;
	private bool isColliding = false;	// used to prevent issues arising if OnTriggerEnter is called more than once in an update cycle
    protected List<Type> effects = new List<Type>();

	/// <summary>
	/// Set a given tower as the target for this projectile. The projecile will constantly move towards this target.
	/// </summary>
	/// <param name="target">Transform of the target [creep] gameobject.</param>
	/// <param name="towerManager">TowerManager class that spawned this projectile.</param>
	public void SetTarget(Transform target, TowerManager towerManager)
	{
		this.target = target;
		hasTarget = true;
		this.towerManager = towerManager;
	}

	/// <summary>
	/// Initialisation
	/// </summary>
	void Start()
	{
        renderer.material.color = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
	}

	/// <summary>
	/// Called once every frame. Moves the projectile towards it's target. 
	/// If it discovers the target no longer exists, the target [creep] must have been destroyed by something else - this projectile no longer has a target, so just remove it.
	/// </summary>
	void Update()
	{
		isColliding = false;
		if (target == null)
		{
			if (hasTarget)
			{
				Destroy(gameObject);
			}
			return;
		}
		transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * Speed);
	}

	/// <summary>
	/// Called when this projectile hits the collider of another game object. (e.g. the target creep)
	/// </summary>
	/// <param name="collider"></param>
	void OnTriggerEnter(Collider collider)
	{
		if (isColliding) return;
		isColliding = true;
		if (target != null && collider != null)
		{
			if (target.gameObject == collider.gameObject && collider.tag == "Creep")
			{
                foreach (Type effectType in effects)
                {
                    Debug.Log(effectType.Name);
                    
                    EffectController effect = target.gameObject.GetComponent(effectType.Name) as EffectController;

                    if (effect == null)
                    {
                        target.gameObject.AddComponent(effectType.Name);
                    }
                    else
                    {
                        effect.Reset();
                    }
                }

                towerManager.PlayerController.Game.CreepHitByProj(collider.gameObject ,this.Damage);
			}
		}

		Destroy(gameObject);
	}
}
