﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Controls the towers targeting system
/// </summary>
public abstract class TowerController : MonoBehaviour
{
	public string Name;
	public int Cost;
	public GameObject Projectile;
	public float ProjectileCoolDown;
	float lastFire = 0;

	LinkedList<GameObject> targets = new LinkedList<GameObject>();
	public GameObject target;

	protected Transform _base;
	protected Transform _turret;
	protected Transform _emitter;

	public TowerManager Manager;
	private TowerNetworker Networker;

	public bool Upgraded = false;	// Not ideal - just smoke and mirrors

    /// <summary>
    /// Sets all the towers components as children of the tower prefab
    /// </summary>
	void Awake()
	{
		Networker = GetComponent<TowerNetworker>();
		foreach (Transform child in transform.GetComponentsInChildren<Transform>())
		{
			switch (child.name)
			{
				case "Base":
					_base = child;
					break;
				case "Turret":
					_turret = child;
					break;
				case "Emitter":
					_emitter = child;
					break;
			}
		}
	}

    /// <summary>
    /// Deals with the tower aiming towards to target
    /// </summary>
	void Update()
	{
        // If there is no target wait for the next one
		while (target == null && targets.Count > 0)
		{
			target = targets.Last.Value;
			targets.RemoveLast();
		}

		if (target)
		{
			_turret.rotation = Quaternion.LookRotation(target.transform.position - _turret.transform.position);
			if (lastFire + ProjectileCoolDown < Time.time)
			{
				lastFire = Time.time;
				Networker.FireProjectile(_emitter.position, target);
			}
		}
	}

    /// <summary>
    /// If a creep enters the range of the tower add it to the list of targets
    /// </summary>
    /// <param name="other"></param>
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Creep")
		{
			targets.AddFirst(other.gameObject);
		}
	}

    /// <summary>
    /// Remove any targets that leave the range of the tower
    /// </summary>
    /// <param name="other"></param>
	void OnTriggerExit(Collider other)
	{
		if (other.gameObject == target)
		{
			target = null;
		}
		targets.Remove(other.gameObject);
	}
}
