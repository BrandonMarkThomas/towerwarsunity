using UnityEngine;
using System.Collections;

/// <summary>
/// Handles the end goal for the creeps.
/// </summary>
public class LaneGoalController : MonoBehaviour
{
	protected LaneController laneController;

	/// <summary>
	/// Start this instance of the lane controller.
	/// </summary>
	void Start()
	{
		laneController = GetComponentInParent<LaneController>();
	}

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="collider">Collider.</param>
	void OnTriggerEnter(Collider collider)
	{
		laneController.GameController.CreepGoalReached(collider.gameObject);
	}
}