﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Manages interation between the player and towers and construction of towers
/// </summary>
public class TowerManager : MonoBehaviour
{
	public PlayerController PlayerController;
	private Dictionary<Vector3, GameObject> towerMap;

    /// <summary>
    /// Gets the player controller and creates a towermap for mapping towers positions within the lane
    /// </summary>
	void Start()
	{
		PlayerController = GetComponent<PlayerController>();
		towerMap = new Dictionary<Vector3, GameObject>();
	}
    /// <summary>
    /// Checks that the player has enough money to build the tower
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
	public bool CheckCanBuild(int index)
	{
		TowerController towerController = PlayerController.Game.Prefabs.Towers[index].GetComponent<TowerController>();
		return PlayerController.PlayerInfo.Money >= towerController.Cost;
	}

    /// <summary>
    /// Checks if the tower can be upgraded based on its upgrade history and player money
    /// </summary>
    /// <param name="tower"></param>
    /// <param name="upgradeCost"></param>
    /// <returns>True if the player has enough money and the tower is not already upgraded otherwise false</returns>
	public bool CheckCanUpgrade(GameObject tower, int upgradeCost){
		TowerController towerController = tower.GetComponent<TowerController>();
		if (towerController.Upgraded) return false;

		return PlayerController.PlayerInfo.Money >= upgradeCost;
	}

    /// <summary>
    /// Deduct money from the player equivalent to the cost of the tower
    /// </summary>
    /// <param name="towerPrefab"></param>
	public void DeductCost(TowerController towerPrefab)
	{
		PlayerController.PlayerInfo.Money -= towerPrefab.Cost;
	}

    /// <summary>
    /// Instantiates the tower prefab if the player is offline i.e not multiplayer
    /// Otherwise instantiates the tower prefab over the Photon Network
    /// Sets the towers parent to the Towers gameobject, adds to a the tower map 
    /// and scans the grid again for pathfinding, lastly increments towers built for achievement purposes
    /// </summary>
    /// <param name="index"></param>
    /// <param name="position"></param>
	public void BuildTower(int index, Vector3 position)
	{
		GameObject instance;
		GameObject prefab = PlayerController.Game.Prefabs.Towers[index];
		if (PlayerController.Game.Offline)
		{
			instance = Instantiate(prefab, position, Quaternion.identity) as GameObject;
		}
		else
		{
			string prefabName = prefab.name;
			instance = PhotonNetwork.Instantiate(prefabName, position, Quaternion.identity, 0);
		}
		instance.GetComponent<TowerController>().Manager = this;
		instance.transform.parent = PlayerController.Lane.TowerObjs.transform;
		towerMap.Add(position, instance);
		AstarPath.active.Scan();

		PlayerController.Achievements.IncrementTowersBuilt(index);
	}

    /// <summary>
    /// Creates a mock tower and creep to check for an invalid build placement
    /// </summary>
    /// <param name="index"></param>
    /// <param name="towerPos"></param>
	public void BuildPhantom(int index, Vector3 towerPos)
	{

		Vector3 creepPos = PlayerController.Lane.GetCreepSpawnPosition();
		GameObject towerInstance = Instantiate(Resources.Load("PhantomTower"), towerPos, Quaternion.identity) as GameObject;
		GameObject creepInstance = Instantiate(Resources.Load("PhantomCreep"), creepPos, Quaternion.identity) as GameObject;

		towerMap.Add(towerPos, towerInstance);
		PhantomCreepController controller = creepInstance.GetComponent<PhantomCreepController>();
		controller.towerManager = PlayerController.Towers;
		controller.towerInstance = towerInstance;
		controller.index = index;
		controller.SetGoal(PlayerController.Lane.GoalArea, true);
	}
    
	// Checking if a tower can be built had to be split into two methods "BuildPhantom"
	// and "CatchPhantom". BuildPhantom is used to instatiate the phatom tower and creep.
	// Due to Asynchronous methods, CatchPhantom is called from within the PhantomCreepController
	// and is used to check whether the path created by the phantom creep is valid.
	/// <summary>
	/// Checks to see if the tower placement is valid by checking the paths validity
	/// </summary>
	/// <param name="towerInstance"></param>
	/// <param name="creepInstance"></param>
    public void CatchPhantom(GameObject towerInstance, GameObject creepInstance)
	{

		int index = creepInstance.GetComponent<PhantomCreepController>().index;
		Vector3 pos = towerInstance.transform.position;

		TowerController towerControllerPrefab = PlayerController.Game.Prefabs.Towers[index].GetComponent<TowerController>();
		if (creepInstance.GetComponent<PhantomCreepController>().validPath)
		{
			DeductCost(towerControllerPrefab);
			RemovePhantom(towerInstance);
			GameObject.Destroy(creepInstance);
			BuildTower(index, pos);
		}
		else
		{
			RemovePhantom(towerInstance);
			GameObject.Destroy(creepInstance);
			Debug.Log("invalid path, can't build tower here!");
		}
	}

    /// <summary>
    /// Remove the phantom tower from the game once checks are complete 
    /// </summary>
    /// <param name="towerInstance"></param>
	public void RemovePhantom(GameObject towerInstance)
	{
		towerInstance.layer = 3;
		AstarPath.active.UpdateGraphs(towerInstance.collider.bounds);
		towerMap.Remove(towerInstance.transform.position);
		GameObject.Destroy(towerInstance);
	}

    /// <summary>
    /// Used to check if the tower map currently has a tower in the position
    /// </summary>
    /// <param name="pos"></param>
    /// <returns>Tower GameObject if one exists in the position null otherwise</returns>
	public GameObject GetExisting(Vector3 pos)
	{
		if (towerMap.ContainsKey(pos))
			return towerMap[pos];
		return null;
	}

    /// <summary>
    /// Removes a tower based in its position in the tower map
    /// </summary>
    /// <param name="pos"></param>
	public void RemoveTower(Vector3 pos)
	{
		if (towerMap.ContainsKey(pos))
		{
			GameObject tower = towerMap[pos];
			RemoveTower(tower);
		}
	}

    /// <summary>
    /// Removes a tower based on the gameobject reference
    /// </summary>
    /// <param name="tower"></param>
	private void RemoveTower(GameObject tower)
	{
		towerMap.Remove(tower.transform.position);
		PhotonNetwork.Destroy(tower);
	}
}
