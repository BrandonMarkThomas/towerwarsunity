﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Network helper class that implements various network methods. Attached to MainController, so will always exist in Singleton.
/// See the Multiplayer/Network outline doc for more information relating to Photon, and it's implementation in this project.
/// </summary>
public class NetworkController : MonoBehaviour 
{
	private RoomInfo[] roomsList;
	
	public bool IsJoiningRoom = false;
	public bool ReadyToStart = false;

	//protected PhotonPlayer opponent;
	protected MainController main;

	/// <summary>
	/// Initialisation
	/// </summary>
	public void Awake()
	{
		main = GetComponent<MainController>();
		PhotonNetwork.automaticallySyncScene = true;
	}
	
	/// <summary>
	/// Connect to the Photon Cloud
	/// </summary>
	public void Connect()
	{
		PhotonNetwork.ConnectUsingSettings("0.1");
	}
	
	/// <summary>
	/// Gets the current status of the attempted connection to Photon.
	/// </summary>
	/// <returns>A status message</returns>
	public string ConnectStatus()
	{
		return PhotonNetwork.connectionStateDetailed.ToString();
	}
	
	/// <summary>
	/// Checks if connected to Photon
	/// </summary>
	/// <returns>True if connected</returns>
	public bool IsConnected()
	{
		return PhotonNetwork.connected;
	}
	
	/// <summary>
	/// Checks if client is connected to a room inside the game lobby.
	/// </summary>
	/// <returns>Ture if in a room</returns>
	public bool InRoom()
	{
		return PhotonNetwork.inRoom;
	}
	
	/// <summary>
	/// Checks if the current room the client is connected to, is full.
	/// </summary>
	/// <returns>True if room is full</returns>
	public bool RoomFull()
	{
		return PhotonNetwork.room.playerCount == PhotonNetwork.room.maxPlayers;
	}
	
	/// <summary>
	/// Checks if client is master client in this game room.
	/// </summary>
	/// <returns>True if master client</returns>
	public bool IsRoomMaster()
	{
		return PhotonNetwork.isMasterClient;
	}
	
	/// <summary>
	/// Creates a room inside the game lobby
	/// </summary>
	/// <param name="name">Name of this game</param>
	public void CreateRoom(string name)
	{
		PhotonNetwork.CreateRoom(name, true, true, 2);
		IsJoiningRoom = true;
	}
	
	/// <summary>
	/// Loads a new game, depending on the game mode selected by the user. Game scene is loaded via PhotonNetwork.LoadLevel, so that the level loaded is synchronous.
	/// </summary>
	/// <param name="offline"></param>
	public void LoadGame(bool offline)
	{
		if (offline && IsConnected())
		{
			PhotonNetwork.Disconnect();
		}
		PhotonNetwork.offlineMode = offline;
		if (!offline)
		{
			// now that the game is about to start, close the room
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
		}
		PhotonNetwork.LoadLevel("Game");
	}
	
	/// <summary>
	/// Gets a list of all rooms available in this game lobby.
	/// </summary>
	/// <returns>An array of RoomInfo objects</returns>
	public RoomInfo[] GetRooms()
	{
		return roomsList;
	}
	
	/// <summary>
	/// Gets the name of the room the client is currently connected to.
	/// </summary>
	/// <returns>The room name</returns>
	public string getRoomName()
	{
		return PhotonNetwork.room.name;
	}
	
	/// <summary>
	/// Refreshes the room list of this game lobby
	/// </summary>
	public void Refresh()
	{
		roomsList = PhotonNetwork.GetRoomList();
	}
	
	/// <summary>
	/// Joins a given room in the game lobby
	/// </summary>
	/// <param name="name"></param>
	public void JoinRoom(string name)
	{
		PhotonNetwork.JoinRoom(name);
		IsJoiningRoom = true;
	}
	
	/// <summary>
	/// Make the current client leave it's room
	/// </summary>
	public void LeaveRoom()
	{
		PhotonNetwork.LeaveRoom();
	}

	/// <summary>
	/// Game has finished - loads back the main menu scene
	/// </summary>
	public void ReturnToMainMenu()
	{
		PhotonNetwork.LoadLevel("MainMenu");
		PhotonNetwork.Disconnect();
		main.AIMode = false;
	}
	
	/// <summary>
	/// PhotonMethod. Once a client joins the lobby for this game, refresh the room list.
	/// </summary>
	void OnJoinedLobby()
	{
		Refresh();
	}
	
	/// <summary>
	/// PhotonMethod. Once a client joins a room, set the properties of this client's PhotonPlayer object to be ready
	/// </summary>
	void OnJoinedRoom()
	{
		IsJoiningRoom = false;
		// load ready is used to synchronise when all clients have loaded the game - set it now so it isn't null for clients who haven't loaded yet
		PhotonNetwork.player.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() { { Consts.LoadReady, false } });
		
		// TODO: set more custom player properties/implement more PhotonPlayer stuff?
		//PhotonNetwork.player.name = "Matt";
	}
	
	/// <summary>
	/// PhotonMethod. Called if the client was unable to create a room - this is most likely due to a duplicate name.
	/// </summary>
	void OnPhotonCreateRoomFailed()
	{
		Debug.Log("Failed to create room - a room of this name probably already exists?");
		IsJoiningRoom = false;
	}
}
