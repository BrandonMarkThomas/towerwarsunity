﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Manages the creeps for each player. One creep manager for each player.
/// </summary>
public class CreepManager : MonoBehaviour
{
	public PlayerController PlayerController;
	public List<int> CoolDownMax = new List<int>();
	public List<int> CoolDownStack = new List<int>();

	/// <summary>
	/// Start this instance and initializes the max cool down and the cool down stack for each creep type.
	/// </summary>
	void Start()
	{
		PlayerController = GetComponent<PlayerController>();

		foreach (GameObject prefab in PlayerController.Game.Prefabs.Creeps)
		{
			CoolDownMax.Add(prefab.GetComponent<CreepController>().CoolDownMax);
			CoolDownStack.Add(prefab.GetComponent<CreepController>().CoolDownMax);
		}
	}

	/// <summary>
	/// Checks if this player can spawn a creep (e.g. has enough money, space in creepstack, etc)
	/// </summary>
	/// <param name="index">Index (in the creep prefab list) of the creep in question</param>
	/// <param name="deduct">Boolean to indicate whether to deduct the cost and creepstack as part of this check</param>
	/// <returns>Boolean indicating whether creep can be spawned or not.</returns>
	public bool CheckCanSpawn(int index, bool deduct)
	{
		CreepController creepController = PlayerController.Game.Prefabs.Creeps[index].GetComponent<CreepController>();
		if (CoolDownStack[index] > 0 && PlayerController.PlayerInfo.Money >= creepController.Cost && !PlayerController.Game.InitialCreepCoolDown)
		{
			if (deduct)
			{
				DeductCosts(index, creepController);
			}
			return true;
		}
		return false;
	}

	/// <summary>
	/// Deducts the cost of the creep being spawned from the player. Keeps track of the creep stack and cool down.
	/// </summary>
	/// <param name="index">Index (in the cool down stack) of the creep in question</param>
	/// <param name="creepController">Creep controller.</param>
	private void DeductCosts(int index, CreepController creepController)
	{
		CoolDownStack[index]--;
		StartCoroutine(StartCoolDown(creepController.CoolDownRate, index));
		PlayerController.PlayerInfo.Money -= creepController.Cost;
	}

	/// <summary>
	/// Starts the cool down.
	/// </summary>
	/// <returns>The cool down.</returns>
	/// <param name="coolDownRate">Cool down rate.</param>
	/// <param name="index">Index (in the cool down stack) of the creep in question.</param>
	IEnumerator StartCoolDown(float coolDownRate, int index)
	{
		yield return new WaitForSeconds(coolDownRate);
		CoolDownStack[index]++;
	}

	/// <summary>
	/// Spawns the creep in the lane.
	/// </summary>
	/// <returns>The creep instance</returns>
	/// <param name="index">Index (in the creep prefab list) of the creep being spawned.</param>
	/// <param name="position">Position where the creep will be spawned in the lane</param>
	public GameObject SpawnCreep(int index, Vector3 position)
	{
		GameObject instance;
		GameObject prefab = PlayerController.Game.Prefabs.Creeps[index];
		if (PlayerController.Game.Offline)
		{
			instance = Instantiate(prefab, position, Quaternion.identity) as GameObject;
		}
		else
		{
			string prefabName = prefab.name;
			instance = PhotonNetwork.Instantiate(prefabName, position, Quaternion.identity, 0);
		}

		instance.GetComponent<CreepController>().prefabIndex = index;
		CreepController creepController = instance.GetComponent<CreepController>();
		creepController.Manager = this;
		return instance;
	}
}
