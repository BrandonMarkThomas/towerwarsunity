﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Handles the achievements earnt by each player throughout the game. 
/// </summary>
public class AchievementsController : MonoBehaviour {

	protected List<int> creepTypesKilled = new List<int> ();
	protected List<int> towerTypesBuilt = new List<int> ();
	public PlayerController PlayerController;

	/// <summary>
	/// Initialize the lists that count the types and number of types of towers that the user has built and creeps that the user has killed.
	/// </summary>
	void Start () {

		PlayerController = GetComponent<PlayerController>();
		List<GameObject> towerPrefabs = PlayerController.Game.Prefabs.Towers;
		List<GameObject> creepPrefabs = PlayerController.Game.Prefabs.Creeps;

		// set initial values to 0 for each type of creep
		for(int i = 0; i < creepPrefabs.Count; i++) 
		{
			creepTypesKilled.Add(0);
		}
		// set initial values to 0 for each type of tower
		for(int i = 0; i < towerPrefabs.Count; i++) 
		{
			towerTypesBuilt.Add(0);
		}
		int test = creepTypesKilled.Count;
		Debug.Log ("creep prefab count" + test.ToString());
	}

	/// <summary>
	/// Increments the towers built.
	/// </summary>
	/// <param name="index">Index for tower type</param>
	public void IncrementTowersBuilt(int index){
		towerTypesBuilt [index] += 1;
	}

	/// <summary>
	/// Increments the creeps killed.
	/// </summary>
	/// <param name="index">Index for creep type</param>
	public void IncrementCreepsKilled(int index){
		creepTypesKilled [index] += 1;
	}

	/// <summary>
	/// Prints the achievements in the console at the end of the game
	/// </summary>
	public void PrintAchievements(){

		int killed = 0;
		for(int i = 0; i < creepTypesKilled.Count; i++){
			killed += creepTypesKilled[i];
			Debug.Log ("type " + i + " creeps killed: " + creepTypesKilled[i]);
		}
		Debug.Log ("Total creeps killed: " + killed);

		int built = 0;
		for(int i = 0; i < towerTypesBuilt.Count; i++){
			built += towerTypesBuilt[i];
			Debug.Log ("type " + i + " towers built: " + towerTypesBuilt[i]);
		}
		Debug.Log ("Total towers built: " + built);
	}
}
