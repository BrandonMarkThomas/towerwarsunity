﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Determines whether an effect is either an instantateous cast or requires a target before being cast.
/// </summary>
public enum EffectType
{
    Instant, // Spell is cast on click
    Target // Spell is targeted, at a Creep, Tower, or Lane
}

/// <summary>
/// Determines the type of target the spell affects.
/// </summary>
[FlagsAttribute]
public enum EffectTargetType
{
    Creep = 1, // The player must select a creep to cast the spell on
    Lane = 2, // The player must select a lane to cast the spell on
    Tower = 4 // The player must select a tower to cast the spell on
}

/// <summary>
/// Determins if the spell will target the current player or the target player.
/// </summary>
[FlagsAttribute]
public enum EffectTarget
{
    Offensive = 1, // Targets units of EffectTargetType in the TargetPlayer's lane
    Defensive = 2 // Targets units of EffectTargetType in the CurrentPlayer's lane
}

/// <summary>
/// Maintains list of spells for a player.
/// </summary>
public class SpellManager : MonoBehaviour
{
	/// <summary>
	/// Available spells. (Hard coded)
	/// </summary>
	public IEffectInfo[] AvailableSpells
	{
		get
		{
			return new IEffectInfo[]
            {
                new EffectInfo<SpeedDrainEffect>("Speed Drain", "Decrease the speed of one of your enemies creeps", EffectType.Target, EffectTarget.Defensive, EffectTargetType.Creep),
                new EffectInfo<SpeedBoostEffect>("Mass Speed Boost", "Increase the speed of all creeps in the target players lane", EffectType.Instant, EffectTarget.Offensive, EffectTargetType.Creep)
            };
		}
	}

	/// <summary>
	/// Used by GUI to get the list of availble spells
	/// </summary>
	public GUIContent[] SpellGUIList
	{
		get
		{
			List<GUIContent> elements = new List<GUIContent>();

			foreach (IEffectInfo info in AvailableSpells)
			{
				elements.Add(new GUIContent(info.Name, info.Description));
			}

			return elements.ToArray();
		}
	}

	private GameController gameController;
	private PlayerController currentPlayer { get { return gameController.CurrentPlayerController; } }
	private PlayerController targetPlayer { get { return gameController.TargetPlayerController; } }
	private int creepLayerMask;
	private bool casting = false;

	/// <summary>
	/// Initialisation
	/// </summary>
	void Start()
	{
		gameController = GetComponent<PlayerController>().Game;
		creepLayerMask = 1 << LayerMask.NameToLayer("Creeps");
	}

	/// <summary>
	/// Called once per frame
	/// </summary>
	void Update()
	{
		if (currentPlayer.GMode != GameMode.Spells || gameController.GUI.SpellIndex < 0 || gameController.GUI.SpellIndex >= AvailableSpells.Length)
		{
			return;
		}

		IEffectInfo info = AvailableSpells[gameController.GUI.SpellIndex];

		if (info.Type == EffectType.Instant)
		{
			List<GameObject> targets = new List<GameObject>();

			if ((info.TargetType & EffectTargetType.Creep) == EffectTargetType.Creep)
			{
				List<CreepController> creepTargets = new List<CreepController>();

				if ((info.Target & EffectTarget.Defensive) == EffectTarget.Defensive)
				{
					creepTargets.AddRange(currentPlayer.Lane.GetComponentsInChildren<CreepController>());
				}
				else if ((info.Target & EffectTarget.Offensive) == EffectTarget.Offensive)
				{
					creepTargets.AddRange(targetPlayer.Lane.GetComponentsInChildren<CreepController>());
				}

				foreach (CreepController creep in creepTargets)
				{
					targets.Add(creep.gameObject);
				}
			}

			if(gameController.Offline)
			{
				foreach(GameObject target in targets)
				{
					info.Cast(target);
				}
			}
			else
			{
				List<int> viewIDs = new List<int>();

				foreach (GameObject target in targets)
				{
					PhotonView targetView = target.GetComponent<PhotonView>();

					if(targetView.isMine)
					{
						info.Cast(target);
					}
					else
					{
						viewIDs.Add (targetView.viewID);
					}
				}

				gameController.View.RPC("RemoteCast", PhotonTargets.Others, new object[] { gameController.GUI.SpellIndex, viewIDs.ToArray() });
			}

			gameController.GUI.SpellIndex = -1;
		}
		else
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000, creepLayerMask))
			{
				info.Cast(hit.collider.gameObject);
			}
		}
	}
}

/// <summary>
/// Abstract effect info container.
/// </summary>
public interface IEffectInfo
{
    string Name { get; }
    string Description { get; }
    EffectType Type { get; }
    EffectTarget Target { get; }
    EffectTargetType TargetType { get; }
   
    void Cast(GameObject gameObject);
}

/// <summary>
/// Less abstract verstion of effect info.
/// </summary>
/// <typeparam name="SpellClass">The effect class that will be casted.</typeparam>
public class EffectInfo<SpellClass> : IEffectInfo where SpellClass : EffectController
{
    public string Name { get; protected set; }
    public string Description { get; protected set; }
    public EffectType Type { get; private set; }
    public EffectTarget Target { get; private set; }
    public EffectTargetType TargetType { get; private set; }

    public EffectInfo(string name, string description, EffectType type, EffectTarget target, EffectTargetType targetType)
    {
        Name = name;
        Description = description;
        Type = type;
        Target = target;
        TargetType = targetType;
    }

	/// <summary>
	/// Called by the SpellManager during the update method.
	/// </summary>
	/// <param name="gameObject">Target of the spell.</param>
    void IEffectInfo.Cast(GameObject gameObject)
    {
        gameObject.AddComponent<SpellClass>().SetInfo(this);
    }
}