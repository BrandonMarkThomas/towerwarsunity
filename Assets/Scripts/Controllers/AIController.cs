﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Controller class used to control the basic AI in "Versus AI" mode.
/// </summary>
public class AIController : MonoBehaviour
{
	protected PlayerController playerController { get { return this.gameController.TargetPlayerController; } }
	protected PlayerController targetPlayerController { get { return this.gameController.CurrentPlayerController; } }
	protected GameController gameController;
	public PrefabController Prefabs { get; private set; }
	protected float nextCreepWave, nextTowerBuild;
	protected int towersBuilt = 0;
	protected int maxCreepIndex;
	protected Queue<Vector3> towerLocations = new Queue<Vector3>();

	/// <summary>
	/// Used for initialization
	/// </summary>
	void Start()
	{
		gameController = GetComponent<GameController>();
		Prefabs = GetComponent<PrefabController>();
		maxCreepIndex = Prefabs.Creeps.Count;

		nextCreepWave = Time.time + 5;

		Transform[] placements = playerController.Lane.Placements.GetComponentsInChildren<Transform>();

		List<Transform> sortedPlacements = new List<Transform>();
		sortedPlacements.AddRange(placements);

		sortedPlacements.Sort(delegate(Transform value1, Transform value2)
		{
			if (value1.position.z < value2.position.z)
			{
				return -1;
			}
			else if (value1.position.z == value2.position.z)
			{
				if (value1.position.x < value2.position.x)
				{
					return -1;
				}
				else if (value1.position.x == value2.position.x)
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
			else
			{
				return 1;
			}
		});

		// Remove parent objects transform
		sortedPlacements.Remove(playerController.Lane.Placements.GetComponent<Transform>());

		//Debug.Log(sortedPlacements.Count);

		int row = -1;
		int column = 0;
		int count = 0;

		foreach (Transform placement in sortedPlacements)
		{
			column = count % 9;

			if (column == 0)
			{
				row++;
			}

			switch (row % 4)
			{
				case 1:
					// Ignore the first in this lane
					if (column == 0)
					{
						break;
					}

					towerLocations.Enqueue(placement.position);
					placement.renderer.material.color = Color.blue;
					break;
				case 3:
					// Ignore the last in this lane
					if (column == 8)
					{
						break;
					}

					towerLocations.Enqueue(placement.position);
					placement.renderer.material.color = Color.cyan;
					break;
				case 0:
				case 2:
					// Dont build in these lanes
					break;
			}

			count++;
		}

		//Debug.Log(column);
		//Debug.Log(row);
		//Debug.Log(count);
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		// If have enough money, and there is no tower on the next tile, build a tower on the next tile
		int towerIndex = 0;
		TowerController towerController = gameController.Prefabs.Towers[towerIndex].GetComponent<TowerController>();

		if (towerLocations.Count > 0 && (Time.time >= nextTowerBuild) && playerController.PlayerInfo.Money >= towerController.Cost)
		{
			playerController.Towers.BuildPhantom(towerIndex, towerLocations.Dequeue());
			towersBuilt++;

			nextTowerBuild = Time.time + towersBuilt;
		}

		// Send a creep wave every random period of time (e.g. between 5-10 seconds)
		if (Time.time >= nextCreepWave)
		{
			SendCreepWave();
			nextCreepWave = Time.time + Random.Range(5, 10);
		}
	}

	/// <summary>
	/// Send a series of creeps to the target (human) player. The amount and type of creeps is random.
	/// </summary>
	void SendCreepWave()
	{
		int waveSize = Random.Range(5, 10);

		for (int i = 0; i < waveSize; i++)
		{
			int creepIndex = Random.Range(0, maxCreepIndex);
			if (playerController.Creeps.CheckCanSpawn(creepIndex, true))
			{
				gameController.SpawnCreep(creepIndex, (int)targetPlayerController.Player);
			}
			else
			{
				break;
			}
		}
	}
}
