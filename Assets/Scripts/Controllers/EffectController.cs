using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for all effects.
/// </summary>
public abstract class EffectController : MonoBehaviour
{
	public PlayerController CastingPlayer { get; private set; }
	public PlayerController TargetPlayer { get; private set; }
	public float StartTime { get; private set; }
	public EffectTargetType TargetType { get; private set; }
	public IEffectInfo Info { get; private set; }
	public CreepController Creep { get; private set; }
	public TowerController Tower { get; private set; }
	public LaneController Lane { get; private set; }

	/// <summary>
	/// Called by the SpellManager. Attaches configuration for the effect.
	/// </summary>
	/// <param name="info"></param>
	public void SetInfo(IEffectInfo info)
	{
		if (Info == null)
		{
			Info = info;
		}
	}

	/// <summary>
	/// When an effect is applied to an object that already has this effect, this is called to reset the effect timer to the full duration of the effect (again).
	/// </summary>
	public void Reset()
	{
		StartTime = Time.time;
	}

	/// <summary>
	/// Initialisation
	/// </summary>
	void Start()
	{
		StartTime = Time.time;

		if ((Lane = gameObject.GetComponent<LaneController>()) != null)
		{
			TargetType = EffectTargetType.Lane;
		}
		else if ((Creep = gameObject.GetComponent<CreepController>()) != null)
		{
			TargetType = EffectTargetType.Creep;
		}
		else if ((Tower = gameObject.GetComponent<TowerController>()) != null)
		{
			TargetType = EffectTargetType.Tower;
		}
		else
		{
			throw new UnityException("Unknown effect target type");
		}

		EffectStart();
	}

	/// <summary>
	/// Called once per frame
	/// </summary>
	public void Update()
	{
		EffectUpdate();
	}

	/// <summary>
	/// Overwritten by concrete effect classes.
	/// </summary>
	protected virtual void EffectStart()
	{

	}

	/// <summary>
	/// Overwritten by concrete effect classes.
	/// </summary>
	protected virtual void EffectUpdate()
	{

	}
}