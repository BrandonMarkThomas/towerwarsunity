﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Overall container class for each player in the game.
/// </summary>
public class PlayerController : MonoBehaviour {

	public GameObject LaneObj { get; private set; }
	public LaneController Lane { get; private set; }

	public Player Player;
	public PlayerInfoController PlayerInfo { get; private set; }

	public GameController Game { get; private set; }
	public TowerManager Towers { get; private set; }
	public CreepManager Creeps { get; private set; }

	public AchievementsController Achievements { get; private set; } 
	public GameMode GMode { get; private set; }
	public TowerMode TMode { get; private set; }
	public SpellManager Spells { get; private set; }

	/// <summary>
	/// Initialisation
	/// </summary>
	void Awake()
	{
		Game = GameObject.Find("GameController").GetComponent<GameController>();
		PlayerInfo = GetComponent<PlayerInfoController>();
		Towers = GetComponent<TowerManager>();
		Creeps = GetComponent<CreepManager>();
		Achievements = GetComponent<AchievementsController> ();
		Spells = gameObject.AddComponent<SpellManager> ();

		GMode = GameMode.Construction;
		TMode = TowerMode.Build;
	}

	/// <summary>
	/// Instantiate a lane prefab for this player.
	/// </summary>
	/// <param name="lanePrefab">The lane prefab gameobject</param>
	/// <param name="position">The position to instantiate this lane at.</param>
	public void CreateLane(GameObject lanePrefab, Vector3 position)
	{
		LaneObj = Instantiate(lanePrefab, position, Quaternion.identity) as GameObject;
		Lane = LaneObj.GetComponent<LaneController>();
		Lane.Player = this.Player;
		LaneObj.transform.parent = this.gameObject.transform;
	}

	/// <summary>
	/// Sets the GameMode that this player is in. Lane placement event handling is also enabled/disabled accordingly.
	/// </summary>
	/// <param name="mode">GameMode to set</param>
	public void SetGameMode(GameMode mode)
	{
		GMode = mode;
		switch (GMode)
		{
			case GameMode.Construction:
				Lane.PlacementController.EventsEnabled = true;
				break;
			default:
				Lane.PlacementController.EventsEnabled = false;
				break;
		}
	}

	/// <summary>
	/// Sets the TowerMode that this player is in.
	/// </summary>
	/// <param name="mode"></param>
	public void SetTowerMode(TowerMode mode)
	{
		TMode = mode;
	}
}
