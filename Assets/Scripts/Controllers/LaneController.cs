﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles the color layout of the lane and the creep start position.
/// </summary>
public class LaneController : MonoBehaviour
{
	public Transform Placements;
	public Transform SpawnArea;
	public Transform GoalArea;
	
	public PlacementController PlacementController { get; private set; }
	public LaneGoalController LaneGoalController { get; private set; }

	public GameController GameController { get; private set; }
	public GameObject CreepObjs { get; private set; }
	public GameObject TowerObjs { get; private set; }

	// for ease of checking which player a placement tile belongs to
	public Player Player;

	/// <summary>
	/// Used for initialization, colors the lane and creates the folder structure that holds creeps and towers.
	/// </summary>
	void Awake()
	{
		GameController = GameObject.Find("GameController").GetComponent<GameController>();
		PlacementController = GetComponent<PlacementController>();
		CreepObjs = new GameObject("Creeps");
		CreepObjs.tag = "Creeps";
		CreepObjs.transform.parent = this.gameObject.transform;
		TowerObjs = new GameObject("Towers");
		TowerObjs.transform.parent = this.gameObject.transform;

        foreach (Renderer placement in Placements.transform.GetComponentsInChildren<Renderer>())
        {
            placement.material.color = Color.gray;
        }

		SpawnArea.renderer.material.color = Color.red;
		GoalArea.renderer.material.color = Color.green;
	}

	/// <summary>
	/// Generates the creep spawn position.
	/// </summary>
	/// <returns>The creep spawn position.</returns>
	public Vector3 GetCreepSpawnPosition()
	{
		// Place the creep in a random position in the spawn area
		float minX = SpawnArea.renderer.bounds.min.x;
		float minZ = SpawnArea.renderer.bounds.min.z;
		float maxX = SpawnArea.renderer.bounds.max.x;
		float maxZ = SpawnArea.renderer.bounds.max.z;

		Vector3 position = new Vector3(Random.Range(minX, maxX), 0.5f, Random.Range(minZ, maxZ));
		return position;
	}

	/// <summary>
	/// Set the goal of the creep to the goal area.
	/// </summary>
	/// <param name="creep">Creep.</param>
	public void SetCreep(GameObject creep)
	{
		creep.transform.parent = CreepObjs.transform;
		CreepController controller = creep.GetComponent<CreepController>();
		if (GameController.Offline)
			controller.SetGoal(GoalArea, true);
		else
			controller.SetGoal(GoalArea, GameController.CurrentPlayer == Player);
	}
}
