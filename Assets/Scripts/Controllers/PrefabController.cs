﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class containing information of each available prefab in the game (e.g. creeps, towers available to send/build)
/// </summary>
public class PrefabController : MonoBehaviour {

	// Lists of possible prefabs that players can create - populated by user in editor
	public List<GameObject> Creeps = new List<GameObject>();
	public List<GameObject> Towers = new List<GameObject>();

	/// <summary>
	/// Creates a GUIContent array with information about each of the towers currently in the prefab list
	/// </summary>
	public GUIContent[] GUITowerList
	{
		get
		{
			List<GUIContent> content = new List<GUIContent>();
			foreach (GameObject prefab in Towers)
			{
				TowerController controller = prefab.GetComponent<TowerController>();
				content.Add(new GUIContent(controller.Name + " (" + controller.Cost + "g)", "Description (isn't displayed currently)"));
			}
			return content.ToArray();
		}
	}
}
