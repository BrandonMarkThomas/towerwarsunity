﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class containing gameplay information relating to the player itself (e.g. health, money)
/// </summary>
public class PlayerInfoController : MonoBehaviour
{
	public int Health = 20;
	public int Money = 50;

	public float IncreaseInterval = 10f;
	public float IncreaseFactor = 1.05f;

	private float lastUpdate;
	private float updateTime;
	private float nextIncrease;
	private float goldPerSecond;

	/// <summary>
	/// Initialisation
	/// </summary>
	void Start()
	{
		lastUpdate = Time.time;
		nextIncrease = Time.time + IncreaseInterval;
		goldPerSecond = 2.5f;
		updateTime = 1 / goldPerSecond;
	}

	/// <summary>
	/// Update is called once per frame. Increases the player's gold - the rate of gold income slowly increases exponentially over the course of a game.
	/// </summary>
	void Update()
	{
		if (Time.time - lastUpdate >= updateTime)
		{
			lastUpdate = Time.time;
			Money++;
		}
		if (Time.time >= nextIncrease)
		{
			goldPerSecond *= IncreaseFactor;
			updateTime = 1 / goldPerSecond;
			nextIncrease = Time.time + IncreaseInterval;
		//	Debug.Log("gps is now: " + goldPerSecond);
		}
	}
}
