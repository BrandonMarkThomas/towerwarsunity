using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Controls the GUI system for the main game scene.
/// </summary>
public class GUIController : MonoBehaviour
{
	protected GameController gameController;

	public int GameModeIndex = 0;
	public int GameModeOld = 0;
	public int TowerModeIndex = -1;
	public int TowerModeOld = -1;
	public int SpellIndex = -1;
	public int SpellIndexOld = -1;

	public int TowerIndex = 0;

	public string PauseMessage;
	public string CoolDownMessage;

	public GUISkin NecroSkin;
	public GUISkin GameSkin;
	private GUIContent[] towers;
	private GUIContent[] spells;
	private string[] gameModes;
	private string[] towerModes;

	// miscellaneous GUI values - TODO: could move to Consts file?
	public const int ButtonHeight = 80;
	public const int LabelHeight = 50;
	public const int BoxMargin = 4;
	// target screen resolution is 1280x800
	//public const int ScreenWidth = 1280;				
	//public const int ScreenHeight = 800;

	public const int HealthBarWidth = 20;
	public const int HeightBarHeight = 12;	//TODO
	public const int GUIMargin = 10;	// distance of any GUI element from edge of screen
	public const int CreepBarHeight = 40;
	public const int SideMenuWidth = 150;

	private Texture healthBarBackground, healthBarForeground;

	/// <summary>
	/// Initialisation
	/// </summary>
	void Start()
	{
		gameController = GetComponent<GameController>();
		towers = gameController.Prefabs.GUITowerList;
		spells = gameController.CurrentPlayerController.Spells.SpellGUIList;
		healthBarBackground = Resources.Load<Texture>("HealthBarBackground");
		healthBarForeground = Resources.Load<Texture>("HealthBarForeground");
		// ideally these strings would be generated from the enum names, but don't want buttons for default modes in there as well - for the moment just manually define
		gameModes = new string[] { "Construction", "Spells" };	//(string[])Enum.GetNames(typeof(GameMode));
		towerModes = new string[] { "Sell (50%)", "Upgrade (10g)" };		//(string[])Enum.GetNames(typeof(TowerMode));
		
		// If more dynamic GUI element sizes are needed, here's how one would update GameSkin values
		//GameSkin.button.fixedHeight = ButtonHeight;
		//GameSkin.label.fixedHeight = LabelHeight;
	}

	/// <summary>
	/// Draws the GUI elements constantly. The majority of the GUI displayed to the player during a game is generated here.
	/// </summary>
	void OnGUI()
	{
		GUI.skin = GameSkin;
		
		// health bars on creeps
		if (Camera.current != null)
		{
			foreach (PlayerController playerGameController in new PlayerController[] { gameController.CurrentPlayerController, gameController.TargetPlayerController })
			{
				foreach (CreepController creep in playerGameController.Lane.CreepObjs.GetComponentsInChildren<CreepController>())
				{
					Vector3 screenPosition = Camera.main.WorldToScreenPoint(creep.transform.position);
					screenPosition.y = Screen.height - (screenPosition.y + 1);
					Rect rect = new Rect(screenPosition.x - (HealthBarWidth / 2), screenPosition.y - 12, HealthBarWidth, 24);

					GUI.BeginGroup(rect);

					if (creep.Health > 0)
					{
						GUI.DrawTexture(new Rect(0, 0, 20, 5), healthBarBackground);
						GUI.DrawTexture(new Rect(0, 0, ((float)20) * (((float)creep.Health) / ((float)creep.MaxHealth)), 5), healthBarForeground);
					}

					GUI.EndGroup();
				}
			}
		}

		// check for pause menu
		if (gameController.Paused)
		{
			PauseMenu();
			return;
		}

		int SideWidth = (int)(0.2 * Screen.width);	// GUI elements down either side of screen are 20% width of the screen
		int MidHeight = Screen.height / 2;
		int SpaceHeight = ButtonHeight / 2 - 10;

		// game mode bar
		GUILayout.BeginArea(new Rect(Screen.width - SideWidth, MidHeight - LabelHeight, SideWidth, MidHeight));
		GUILayout.Label("Health: " + gameController.TargetPlayerController.PlayerInfo.Health.ToString() + "/" + Consts.MaxPlayerHealth, "EnemyHealth");
		GUILayout.Space(SpaceHeight / 2);
		GameModeIndex = GUILayout.SelectionGrid(GameModeIndex, gameModes, 1);
		if (GUI.changed)
		{
			// for deselecting the currently chosen selection
			if (GameModeIndex == GameModeOld)
			{
				GameModeIndex = -1;
			}
		}
		if (GameModeIndex != GameModeOld)
		{
			GameModeOld = GameModeIndex;
			gameController.CurrentPlayerController.SetGameMode((GameMode)GameModeIndex + 1);
		}
		GUILayout.Space(SpaceHeight);
		if (GUILayout.Button("Pause"))
		{
			gameController.Pause(Consts.PauseOwn);
		}
		GUILayout.EndArea();

		// player info
		GUILayout.BeginArea(new Rect(0, 0, SideWidth, 2 * LabelHeight + SpaceHeight));
		GUILayout.BeginVertical();
		GUILayout.Label("Health: " + gameController.CurrentPlayerController.PlayerInfo.Health + "/" + Consts.MaxPlayerHealth);
		GUILayout.Label("Money: " + gameController.CurrentPlayerController.PlayerInfo.Money, "MoneyHighlight");
		GUILayout.EndVertical();
		GUILayout.EndArea();

		// mode expansion bar
		GameMode m = gameController.CurrentPlayerController.GMode;
		if (m != GameMode.Default)
		{
			GUILayout.BeginArea(new Rect(0, 2 * LabelHeight + SpaceHeight, SideWidth, Screen.height - ButtonHeight - 2 + BoxMargin));
			switch (m)
			{
				case GameMode.Construction:
					TowerModeIndex = GUILayout.SelectionGrid(TowerModeIndex, towerModes, 1);
					if (GUI.changed)
					{
						if (TowerModeIndex == TowerModeOld)
						{
							TowerModeIndex = -1;
						}
					}
					if (TowerModeOld != TowerModeIndex)
					{
						TowerModeOld = TowerModeIndex;
						gameController.CurrentPlayerController.SetTowerMode((TowerMode)TowerModeIndex + 1);
					}
					GUILayout.Space(SpaceHeight);
					GUILayout.Label("Build tower:", "SectionLabel", GUILayout.Width(SideWidth));

					if (gameController.CurrentPlayerController.TMode != TowerMode.Build)
					{
						GUI.enabled = false;
					}
					// Ideally would call TowerManager.CanCheckBuild() for each tower and disable if unable to afford.
					// However with the basic Unity GUI, this can not be done easily.
					// Currently the money text will flash red if user attempts to build a tower without having sufficient funds.
					TowerIndex = GUILayout.SelectionGrid(TowerIndex, towers, 1);
					GUI.enabled = true;
					break;
				case GameMode.Spells:
					SpellIndex = GUILayout.SelectionGrid(SpellIndex, spells, 1);
					break;
				default:
					Debug.LogError("Unknown game mode in GUI");
					break;
			}
			GUILayout.EndArea();
		}

		// creep spawn bar
		GUILayout.BeginArea(new Rect(0, Screen.height - ButtonHeight - 2 * BoxMargin, Screen.width, ButtonHeight + 2 * BoxMargin));
		GUILayout.BeginHorizontal("box");
		GUILayout.Label("Send creep: ", "SectionLabel");
		if (gameController.InitialCreepCoolDown)
		{
			GUILayout.FlexibleSpace();
			GUILayout.Label(CoolDownMessage, "SectionLabel");
			GUILayout.FlexibleSpace();
		}
		else
		{
			for (int i = 0; i < gameController.Prefabs.Creeps.Count; i++)
			{
				GameObject creep = gameController.Prefabs.Creeps[i];
				CreepController bc = creep.GetComponent<CreepController>();
				int currCount = gameController.CurrentPlayerController.Creeps.CoolDownStack[i];
				int maxCount = gameController.CurrentPlayerController.Creeps.CoolDownMax[i];
				if (!gameController.CurrentPlayerController.Creeps.CheckCanSpawn(i, false))
				{
					GUI.enabled = false;
				}
				if (GUILayout.Button("[" + currCount + "/" + maxCount + "] " + bc.Name + " (" + bc.Cost + "g)", GUILayout.ExpandHeight(true)))
				{
					int index = gameController.Prefabs.Creeps.IndexOf(creep);
					gameController.SendCreep(index);
				}
				GUI.enabled = true;
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.EndArea();

		// show toggle players button if in singleplayer mode
		GUILayout.BeginArea(new Rect(Screen.width - SideWidth, 0, SideWidth, ButtonHeight));
		if (PhotonNetwork.offlineMode && !gameController.AI)
		{
			if (GUILayout.Button("Switch player"))
			{
				gameController.TogglePlayers();
			}
		}
		GUILayout.EndArea();
	}
	
	/// <summary>
	/// Helper method to display appropriate GUI elements if game is paused.
	/// </summary>
	void PauseMenu()
	{
	//	GUI.skin = NecroSkin;
		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical("window");

		GUILayout.Label(PauseMessage, "GameOver");

		switch (gameController.GameState)
		{
			case PauseState.Loaded:
				break;
			case PauseState.Started:
				if (GUILayout.Button("Resume"))
				{
					gameController.UnPause();
				}
				if (GUILayout.Button("Resign"))
				{
					gameController.SendGameOver((int)gameController.CurrentPlayer);
				}
				break;
			case PauseState.Finished:
				if (GUILayout.Button("Return to menu"))
				{
					gameController.Main.NetworkControl.ReturnToMainMenu();
				}
				break;
			default:
				Debug.LogError("What is this state? " + gameController.GameState);
				break;
		}

		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.EndArea();
	}

	/// <summary>
	/// Used to highlight the money text GUI element briefly. As GUI SelectionGrid elements cannot be disabled specifially, this is used as an alternate measure to inform the user that their action is illegal.
	/// </summary>
	public void ShowInvalidMoney()
	{
		StartCoroutine(HighlightMoney());
	}

	/// <summary>
	/// IENumerator used to wait out the time that the text is red for.
	/// </summary>
	/// <returns>When time is up</returns>
	IEnumerator HighlightMoney()
	{
		GameSkin.customStyles[2].normal.textColor = Color.red;
		yield return new WaitForSeconds(0.2f);
		GameSkin.customStyles[2].normal.textColor = Color.white;
	}
}
