﻿using UnityEngine;
using System.Collections;

public delegate void PlacementEventHandler(Transform placementTile);

/// <summary>
/// Handles the users interaction with the lane. Hover, mouse down etc.
/// </summary>
public class PlacementController : MonoBehaviour {
	
	private int layerMask;
	private bool _eventsEnabled = false;

	public event PlacementEventHandler OnPlacementSelect;
	public event PlacementEventHandler OnPlacementHover;

	/// <summary>
	/// Gets or sets a value indicating whether the placement event is enabled.
	/// </summary>
	/// <value><c>true</c> if events enabled; otherwise, <c>false</c>.</value>
	public bool EventsEnabled
	{
		get { return _eventsEnabled; }
		set { _eventsEnabled = value; }
	}

	/// <summary>
	/// Starts this instance and initializes the placements layer mask.
	/// </summary>
	void Start () {
		layerMask = 1 << LayerMask.NameToLayer("Placements");
	}

	/// <summary>
	/// If the placement event is enabled, determine mouse down or hover.
	/// </summary>
	void Update () {
		if (!EventsEnabled) {
			return;
		}

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 1000, layerMask))
		{
			if(Input.GetMouseButtonDown (0))
			{
				if(OnPlacementSelect != null)
				{
					OnPlacementSelect.Invoke(hit.collider.transform);
				}
			}
			else
			{
				if(OnPlacementHover != null)
				{
					OnPlacementHover.Invoke(hit.collider.transform);
				}
			}
		}
	}
}


