﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Concrete class for the speed boost effect.
/// </summary>
public class SpeedBoostEffect : EffectController {

	protected override void EffectUpdate () {
		Creep.RuntimeSpeed += (Creep.Speed * 5);
	}
}
