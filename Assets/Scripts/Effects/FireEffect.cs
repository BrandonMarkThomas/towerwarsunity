﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Concrete class for the fire effect.
/// </summary>
class FireEffect : EffectController
{
    const float timeToLive = 5;		// how long the effect will last for
    float lastDamage;
    public int BurnDamage = 1;
    
    protected override void EffectStart()
    {
        // Add trail renderer
        GameObject fireInstance = (GameObject)Instantiate(GameObject.Find("GameController").GetComponent<GameController>().FireEffect, Creep.transform.position, Creep.transform.rotation);
        fireInstance.transform.parent = gameObject.transform;

        lastDamage = 0;
    }

    protected override void EffectUpdate()
    {
        if (Time.time > (StartTime + timeToLive))
        {
            // Remove trail renderer
            Destroy(this);
        }

        if (Time.time > (lastDamage + 1))
        {
            GameController g = this.Creep.Manager.PlayerController.Game;
            g.CreepHitByProj(this.Creep.gameObject, BurnDamage);
            lastDamage = Time.time;
        }
    }
}

