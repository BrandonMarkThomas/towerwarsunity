﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Concrete class for the speed drain effect.
/// </summary>
public class SpeedDrainEffect : EffectController {

	int timeToLive = 3;

	protected override void EffectUpdate ()
	{
		if(StartTime + timeToLive < Time.time)
		{
			Destroy(this);
		}

		Creep.RuntimeSpeed -= Creep.RuntimeSpeed * 0.5f;
	}
}
