﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Concrete class for the ice effect.
/// </summary>
public class IceEffect : EffectController {

    const float timeToLive = 3;		// how long the effect will last for

    protected override void EffectStart()
    {
        Creep.renderer.material.color = Color.blue;
    }

    protected override void EffectUpdate()
    {        
        Creep.RuntimeSpeed *= 0.66f;

        if (Time.time > (StartTime + timeToLive))
        {
            Creep.renderer.material.color = Color.white;
            Destroy(this);
        }
    }
}
